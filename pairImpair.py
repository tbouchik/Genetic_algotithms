#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 11:18:03 2018

@author: bouchikhi
"""
import random 
import numpy as np
import matplotlib.pyplot as plt

# QUESTION 1
"TABLEAUX DES PROBABILITÉS"
def initProbaJoueur():
    randomInt = random.triangular(0,1)
    return [randomInt, 1.0 - randomInt ]
p1 = initProbaJoueur()
p2 = initProbaJoueur()

"ESPÉRANCES DE GAIN SUR UNE PARTIE"
def esperanceJ1contreJ2(p1, p2, identic= 1): 
    if identic == 1:
        return p1[0]*p2[0]*1 + p1[0]*p2[1]*(-1) +p1[1]*p2[1]*1 + p1[1]*p2[0]*(-1)
    else: 
        return -(p1[0]*p2[0]*1 + p1[0]*p2[1]*(-1) +p1[1]*p2[1]*1 + p1[1]*p2[0]*(-1))

"ESPÉRANCE DE GAIN DE J1 CONTRE UNE POPULATION 2 : S = 1..N2"
def esperanceJ1contrePop2(p1,tableauDesProbaPopulation2, identic =1 ):
    esperanceJ1 = 0
    for k in range(len(tableauDesProbaPopulation2)) :
        esperanceJ1 += esperanceJ1contreJ2(p1, tableauDesProbaPopulation2[k], identic)
    return esperanceJ1

#alpha
"CRÉATION DE POPULATIONS"
def initPopulation(N):
    strategiePop1=[None]* N
    for k in range (0,N):
        strategiePop1[k] = initProbaJoueur()
    return strategiePop1
#beta

"REMPLISSAGE TABLEAU DES ESPERANCES POUR CHACUN DES JOUEURS DES DEUX POPULATIONS"

def esperancePop1ContrePop2(pop1, pop2, identic = 1):
    result = []
    for k in range(len(pop1)):
        result.append(esperanceJ1contrePop2(pop1[k], pop2, identic))
    return result


"RANG_MAX : CALCUL DE L'INDIVIDU DE LA POPULATION 1 AYANT LA MEILLEUR ESPERANCE FACE A LA POPULATION 2"
def rangMaxPopulation(pop1, pop2, identic = 1):
    tableauEsperances = esperancePop1ContrePop2(pop1, pop2, identic)
    return tableauEsperances.index(max(tableauEsperances))

"RANG_MAX_K CALCUL DE L'INDIVIDU DE LA POPULATION_1_K AYANT LA MEILLEUR ESPERANCE FACE A LA POPULATION_2_K-1"
def rangMinMax(pop1, joueur_adverse, identic=1):
    tab = []
    for k in range(len(pop1)):
        tab.append(esperanceJ1contreJ2(pop1[k], joueur_adverse, identic))
    return [tab.index(max(tab)), tab.index(min(tab))]

"MUTATION"
def mutationIndividu(meilleur, em):
    nouvelle_probabilite = 1000
    while nouvelle_probabilite > 1 or nouvelle_probabilite< -1:
        nouvelle_probabilite = random.triangular(meilleur[0] -meilleur[0]*em, meilleur[0] +meilleur[0]*em)
    return [nouvelle_probabilite, 1-nouvelle_probabilite]



def simulation_selection_naturelle(Npop, em, iter_max) : 
    pop1 = initPopulation(Npop)
    pop2 = initPopulation(Npop)
    r0_max_1 = rangMaxPopulation(pop1, pop2, 1)
    r0_max_2 = rangMaxPopulation(pop2,pop1, -1)
    iteration = 0
    esperanceMeilleur1 = []
    esperanceMeilleur2 = []
    while iteration < iter_max : 
        [r_max_1, r_min_1] = rangMinMax(pop1, pop2[r0_max_2], 1)
        [r_max_2, r_min_2] = rangMinMax(pop2, pop1[r0_max_1], -1)
        pop1[r_min_1] = mutationIndividu(pop1[r_max_1], em)
        pop2[r_min_2] = mutationIndividu(pop2[r_max_2], em)
        esperanceMeilleur1.append(esperanceJ1contreJ2(pop1[r_max_1], pop2[r_max_2], 1))
        esperanceMeilleur2.append(esperanceJ1contreJ2(pop1[r_max_1], pop2[r_max_2], -1))
        iteration +=1
    temps = list(range(iter_max))
    plt.plot(temps, esperanceMeilleur1, 'blue', temps, esperanceMeilleur2, 'green')
    plt.show()

#Influence du temps;
N = 10


simulation_selection_naturelle(N, 0.02, 500)

   
