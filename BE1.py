#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 11:18:03 2018

@author: bouchikhi
"""
import numpy as np
import matplotlib.pyplot as plt
import random 

def f(t):
    return -((t**2)*(2+np.sin(10*t))**2)

t1 = np.arange(-1.0, 1.0, 0.01)

plt.plot(t1, f(t1))

#plt.show()

#Question 1
#'Plusieurs maximums locaux'

#Question 2
 
def initPop (Npop) :
    
    result = [0]*Npop
    for k in range (0, Npop -1) :
        result[k] = random.triangular(-1.0,1.0)
    return result

def tri_synchro(list_a_trier, list_index):
    list_a_trier, list_index = (list(t) for t in zip(*sorted(zip(list_a_trier, list_index))))
    return [list_a_trier, list_index]

def grandeMutation():
    
    return random.triangular(-1.0, 1.0)

def petiteMutation(individuMax, tailleMutation):
    
    return individuMax +random.triangular(-tailleMutation, tailleMutation)

def reproductionSexuee(individu, individuMax):
    
    return (individu+individuMax)/2

#Question A
def simulation (Npop, pM, pm, ps, prec):
    tempsMoyen = 0
    "while erreur > prec"
    erreur = 1000
    pop = initPop(Npop)
    f_de_pop=[None]*len(pop)
    "initialisation de l'image de la pop par la fonction"
    for k in range(0,Npop -1):
        f_de_pop[k] = f(pop[k])

    while(erreur > prec) :
        print('prec voulue : ', prec,' & erreur : ',erreur)
        tempsMoyen +=1
        
        f_de_pop, pop = tri_synchro(f_de_pop, pop)

        "GRANDE MUTATION"
        for k in range(0, int(pM*len(pop))) :
            pop[k] = grandeMutation()
            f_de_pop[k] = f(pop[k])
        "PETITE MUTATION"
        for k in range (int(pM*len(pop)), int(pM*len(pop))+ int(pm*len(pop))):
            pop[k] = petiteMutation(pop[-1],0.1)
            f_de_pop[k] = f(pop[k])
            
        "REPRODUCTION SEXUÉE"
        int3 = int(pM*len(pop))+ int(pm*len(pop))
        propS = int(ps*len(pop))
        for k in range(int3, int3 + propS):
            pop[k] = reproductionSexuee(pop[k], pop[-1])
            f_de_pop[k] = f(pop[k])
        "CALCUL ERREUR"
        
        erreur = f_de_pop[-1]-f_de_pop[int(len(pop)*0.90)] 
    print(pop[-1], tempsMoyen)
    return (pop[-1], tempsMoyen )

#Question B1
print ('éxecution B1')
#''population de 10''
Npop =10
pM = 0.8
pm=0
ps=0
M=50
prec =[0]*M
logTempsMoyen1 = [0]*M
for k in range (1, M+1):
    "LES PRÉCISIONS EN ABSCISSE"
    prec[k-1]= pow(10, -8*(k/M))
    "TEMPS MOYEN EN ORDONNÉE"
    logTempsMoyen1[k-1] = simulation(Npop,pM,pm, ps, prec[k-1])[1]

plt.subplot(221)
plt.title('1')
plt.autoscale(True, 'both')

plt.plot(prec, logTempsMoyen1)

'''population de 20'''
Npop=50
for k in range (1, M+1):
    "LES PRÉCISIONS EN ABSCISSE"
    prec[k-1]= pow(10, -8*(k/M))
    "TEMPS MOYEN EN ORDONNÉE"
    logTempsMoyen1[k-1] = simulation(Npop,pM,pm, ps, prec[k-1])[1]
plt.figure(1)
plt.subplot(222)
plt.title('2')
plt.autoscale()

plt.plot(prec, logTempsMoyen1)

#Question B2

Npop =10
pM = 0.8
pm=0.19
ps=0
M=50
prec =[0]*M
logTempsMoyen1 = [0]*M
for k in range (1, M+1):
    "LES PRÉCISIONS EN ABSCISSE"
    prec[k-1]= pow(10, -8*(k/M))
    "TEMPS MOYEN EN ORDONNÉE"
    logTempsMoyen1[k-1] = simulation(Npop,pM,pm, ps, prec[k-1])[1]
plt.subplot(223)
plt.title('3')
plt.autoscale()

plt.plot(prec, logTempsMoyen1)
'''population de 20'''
Npop=20
for k in range (1, M+1):
    "LES PRÉCISIONS EN ABSCISSE"
    prec[k-1]= pow(10, -8*(k/M))
    "TEMPS MOYEN EN ORDONNÉE"
    logTempsMoyen1[k-1] = simulation(Npop,pM,pm, ps, prec[k-1])[1]
plt.subplot(224)
plt.title('4')
plt.autoscale()
plt.plot(prec, logTempsMoyen1)
plt.show()







    
    
    

    
    
        









