#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 11:18:03 2018

@author: bouchikhi
"""
import numpy as np
import matplotlib.pyplot as plt
import random 
'''
def f(t):
    return -((t**2)*(2+np.sin(10*t))**2)

t1 = np.arange(-1.0, 1.0, 0.01)

plt.plot(t1, f(t1))

plt.show()

#Question 1
'Plusieurs maximums locaux''

#Question 2
 
def initPop (Npop) :
    
    result = [0]*Npop
    for k in range (0, Npop -1) :
        result[k] = random.triangular(-1.0,1.0)
    return result

def grandeMutation():
    
    return random.triangular(-1.0, 1.0)

def petiteMutation(individuMax, tailleMutation):
    
    return individuMax +random.triangular(-tailleMutation, tailleMutation)

def reproductionSexuee(individu, individuMax):
    
    return (individu+individuMax)/2

#Question A
def simulation (Npop, pM, pm, ps, prec):
    tempsMoyen = 0
    "while erreur > prec"
    erreur = 1000
    pop = initPop(Npop)
    
    while(erreur > prec) :
        tempsMoyen +=1
        for k in range(0,Npop -1):
            pop[k] = f(pop[k])
        pop = sorted(pop)
        
        "GRANDE MUTATION"
        for k in range(0, int(pM*len(pop))) :
            pop[k] = grandeMutation()
            
        "PETITE MUTATION"
        for k in range (int(pM*len(pop)), int(pM*len(pop))+ int(pm*len(pop))):
            pop[k] = petiteMutation(pop[-1],0.1)
            
        "REPRODUCTION SEXUÉE"
        int3 = int(pM*len(pop))+ int(pm*len(pop))
        propS = int(ps*len(pop))
        for k in range(int3, int3 + propS):
            pop[k] = reproductionSexuee(pop[k], pop[-1])
            
        "CALCUL ERREUR"
        borneInf = pop[0]
        for i in range (0,len(pop)-1):
            if borneInf > pop[i] :
                borneInf = pop[i]
        erreur = pop[-1] -borneInf
    print('temps moyen', tempsMoyen)
    return (pop[-1], tempsMoyen)

#Question B1
print ('éxecution B1')
''population de 10''
Npop =10
pM = 0.8
pm=0
ps=0
M=50
prec =[0]*M
logTempsMoyen1 = [0]*M
for k in range (1, M+1):
    "LES PRÉCISIONS EN ABSCISSE"
    prec[k-1]= pow(10, -8*(k/M))
    "TEMPS MOYEN EN ORDONNÉE"
    logTempsMoyen1[k-1] = np.log10(simulation(Npop,pM,pm, ps, prec[k-1])[1])
plt.figure(1)
plt.subplot(211)
plt.plot(prec, logTempsMoyen1)
''population de 20''
Npop=20

#Question B2

Npop =10
pM = 0.8
pm=0.19
ps=0
M=50
prec =[0]*M
logTempsMoyen1 = [0]*M
for k in range (1, M+1):
    "LES PRÉCISIONS EN ABSCISSE"
    prec[k-1]= pow(10, -8*(k/M))
    "TEMPS MOYEN EN ORDONNÉE"
    logTempsMoyen1[k-1] = np.log10(simulation(Npop,pM,pm, ps, prec[k-1])[1])
plt.subplot(212)
plt.plot(prec, logTempsMoyen1)
''population de 20''
Npop=20
plt.show()
'''

######################################################################
                ############# EXO 2 #############
######################################################################

# QUESTION 1
"TABLEAUX DES PROBABILITÉS"
def initProbaJoueur():
    return [random.triangular(0,1),random.triangular(0,1) ]
p1 = initProbaJoueur()
p2 = initProbaJoueur()

"ESPÉRANCES DE GAIN LORS DES DEUX PRMIERS TOURS"
def esperanceJ1contreJ2(p1, p2): 
    return p1[0]*p2[0]*1 + p1[0]*(1-p2[0])*(-1) +p1[1]*p2[1]*1 + p1[1]*(1-p2[1])*(-1)

"ESPÉRANCE DE GAIN DE J1 CONTRE UNE POPULATION 2 : S = 1..N2"
def esperanceJ1contrePop2(p1,tableauDesProbaPopulation2 ):
    esperanceJ1 = 0
    for k in range(len(tableauDesProbaPopulation2)) :
        esperanceJ1 += p1[0]*tableauDesProbaPopulation2[k][0]*1 + p1[0]*(1-tableauDesProbaPopulation2[k][0])*(-1) +p1[1]*tableauDesProbaPopulation2[k][1]*1 + p1[1]*(1-tableauDesProbaPopulation2[k][1])*(-1)
        return esperanceJ1
    
#QUESTION 2
    
#alpha
"CRÉATION DE POPULATIONS"
def initPopulation(N):
    strategiePop1=[[0,0]]* N
    for k in range (0,N):
        strategiePop1[k][0] = random.triangular(0,1)
        strategiePop1[k][1] = random.triangular(0,1)
    return strategiePop1
#beta

"REMPLISSAGE TABLEAU DES ESPERANCES POUR CHACUN DES JOUEURS DES DEUX POPULATIONS"

def esperancePop1ContrePop2(pop1, pop2):
    result=[0]*len(pop1)
    for k in range(len(pop1)):
        result[k]= esperanceJ1contrePop2(pop1[k], pop2)
    return result

def rangMaxPopulation(pop1, pop2):
    tableauEsperances = esperancePop1ContrePop2(pop1, pop2)
    indexDuMax = 0
    for k in range(1,len(tableauEsperances)):
        if tableauEsperances[k] > tableauEsperances[indexDuMax]:
            indexDuMax = k
    return indexDuMax
'''script'''
N1 = 10
N2 = 10
for k in range (10):
    pop1 = initPopulation(N1)
    pop2 = initPopulation(N2)
    print(rangMaxPopulation(pop1, pop2))




    
    
    

    
    
        









